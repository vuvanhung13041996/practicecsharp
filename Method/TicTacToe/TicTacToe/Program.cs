﻿/*
 * create name space
 * create class
 * create array char game
 * init value for array game = ' ';
 * draw game for broad
 * choose: (codition chooose) iftrue => choose => next turn , nochoose if fault and return text for rechose 
 * => check win? (coditione win)
 * => finish game or enter key for new game;
 */

TicTocToe newgame = new TicTocToe();
newgame.Main();
public class TicTocToe
{
    // switch turn play game
    char personPlay = 'X';
    //create array for game
    char[,] ticTocToe = new char[3, 3];
    public void Main()
    {
        InitialValueForGame();
        bool coditioncancel = true;
        while (coditioncancel)
        {
            drawArrayWithTerminal();
            Console.WriteLine($"turn {personPlay} play");
            //rawbool
            bool rawBool = false;
            int raw = 0;
            while (!rawBool)
            {
                Console.WriteLine("0 <= raw <3");
                rawBool = int.TryParse(Console.ReadLine(), out raw);
            }

            //colbool
            bool colBool = false;
            int col = 0;
            while (!colBool)
            {
                Console.WriteLine("0 <= col <3");
                colBool = int.TryParse(Console.ReadLine(), out col);
            }

            //chooose
            if (coditionLimitIndex(raw, col))
            {
                if (!coditionChoose(raw, col))
                {
                    addValueForArray(raw, col);

                    drawArrayWithTerminal();

                    if (checkWin(raw, col))
                    {
                        Console.WriteLine($"{personPlay} wins");
                        coditioncancel = false;
                    }
                    else if (checkFull(ticTocToe))
                    {
                        Console.WriteLine("alredy full turn choose , game aqual");
                        coditioncancel = false;
                    }
                    else
                    {
                        turnPlay();
                    }
                }
            }
            else
            {
                Console.WriteLine("rechoose becaue you input out of range");
            };
            Console.WriteLine("--------------------------------------------------------------------------------");
        }
    }
    // initial value for array game
    public void InitialValueForGame()
    {
        for (int i = 0; i < ticTocToe.GetLength(0); i++)
        {
            for (int j = 0; j < ticTocToe.GetLength(1); j++)
            {
                ticTocToe[i, j] = ' ';
            }
        }
    }


    // draw game for terminal
    public void drawArrayWithTerminal()
    {
        for (int i = 0; i < ticTocToe.GetLength(0); i++)
        {
            Console.WriteLine("--------------");
            for (int j = 0; j < ticTocToe.GetLength(1); j++)
            {
                Console.Write($"| {ticTocToe[i, j]}");
            }
            Console.WriteLine("|");

        }
    }


    //codition choose
    public bool coditionChoose(int raw, int col)
    {
        bool result = true;
        if (ticTocToe[raw, col] == ' ')
        {
            Console.WriteLine("choosed");
            result = false;
        }
        else
        {
            Console.WriteLine("alredy choosed please rechoose");
        }
        return result;
    }

    //codition index limit
    public bool coditionLimitIndex(int raw, int col)
    {
        bool result = false;
        if (raw >= 0 && raw < 3 && col >= 0 && col < 3)
        {
            Console.WriteLine("within the allowed range");
            result = true;
        }
        else
        {
            Console.WriteLine("without the allowed range please rechoose");
            result = false;
        }
        return result;
    }

    //switch turn
    public void turnPlay()
    {
        personPlay = (personPlay == 'X') ? 'O' : 'X';
    }

    //check win?
    public bool checkWin(int raw, int col)
    {
        bool result = false;
        if (ticTocToe[raw, 0] == personPlay && ticTocToe[raw, 1] == personPlay && ticTocToe[raw, 2] == personPlay)
        {
            result = true;
        }
        else if (ticTocToe[0, col] == personPlay && ticTocToe[1, col] == personPlay && ticTocToe[2, col] == personPlay)
        {
            result = true;
        }
        else if (ticTocToe[0, 0] == personPlay && ticTocToe[1, 1] == personPlay && ticTocToe[2, 2] == personPlay)
        {
            result = true;
        }
        else if (ticTocToe[0, 2] == personPlay && ticTocToe[1, 1] == personPlay && ticTocToe[2, 0] == personPlay)
        {
            result = true;
        }

        return result;
    }

    //check full
    public bool checkFull(char[,] ticTocToe)
    {
        bool result = true;
        for (int i = 0; i < ticTocToe.GetLength(0); i++)
        {
            for (int j = 0; j < ticTocToe.GetLength(1); j++)
            {
                if (ticTocToe[i, j] == ' ')
                {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    //addvalue for array
    public void addValueForArray(int raw, int col)
    {
        ticTocToe[raw, col] = personPlay;
    }
}
