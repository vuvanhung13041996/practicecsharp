﻿int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

bool FuncBinarySearch(int[] arrrr, int number)
{
    bool result = false;
    int left = 0;
    int right = arrrr.GetLength(0) - 1;
    for (int i = left; i <= right; i++)
    {
        int midle = (right + left) / 2;
        if (arrrr[midle] == number)
        {
            result = true;
            break;
        }
        else if (left > right)
        {
            result = false;
            break;
        }
        else if (arrrr[midle] > number)
        {
            right = midle - 1;
        }
        else
        {
            left = midle + 1;
        }
    }
    return result;
}

Console.WriteLine(FuncBinarySearch(array, 2));