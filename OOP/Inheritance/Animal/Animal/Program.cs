﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Main.Func();


class Main
{
    static Cat a = new Cat("SCOTIS", "MEOW", "CAT", "YELLOW");
    static Cat b = new Cat("BANGAL", "MEOW", "CAT", "BUMBLE");
    static Cat c = new Cat("BATU", "MEOW", "CAT", "RED");
    static Dog d = new Dog("POODLE", "WOFF!", "DOG", "BLUE");
    static Dog f = new Dog("CORGI", "WOFF!", "DOG", "PINK");
    static Dog g = new Dog("HUSKY", "WOFF!", "DOG", "GREEN");
    static Animal[] animals = new Animal[6] { a, b, c, d, f, g };
    public static void Func()
    {
        foreach (Animal animal in animals)
        {
            if (animal is Dog)
            {
                animal.Speak();
                var newDog = (Dog)animal;
                newDog.Bark();
            }

            var newCat = animal as Cat;
            if (newCat != null)
            {
                animal.Speak();
                newCat.Meow();
            }
        }
    }
}

class Animal
{
    private string _Name;
    private string _Sound;
    private string _Type;
    public Animal(string name, string sound, string type)
    {
        _Name = name;
        _Sound = sound;
        _Type = type;
    }
    public void Speak()
    {
        Console.WriteLine($"The {_Type} named {_Name} says");
    }
}
class Cat : Animal
{
    public string _FurColor;
    public Cat(string name, string sound, string type, string color) : base(name, sound, type)
    {
        _FurColor = color;
    }
    public void Meow()
    {
        Console.WriteLine("Meow Meow");
    }
}
class Dog : Animal
{
    public string _Breed;
    public Dog(string name, string sound, string type, string Breed) : base(name, sound, type)
    {
        _Breed = Breed;
    }

    public void Bark()
    {
        Console.WriteLine("WOOF! WOOF!");
    }
}


