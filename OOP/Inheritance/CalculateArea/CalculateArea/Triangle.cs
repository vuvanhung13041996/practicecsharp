﻿

class Triangle : Shape
{
    int _first;
    int _second;
    int _third;
    public Triangle(int first, int second, int third)
    {
        _first = first;
        _second = second;
        _third = third;
    }
    public override int CalculateArea()
    {
        var s = (_first + _second + _third) / 2;
        var area = Math.Sqrt((s * (s - _first) * (s - _second) * (s - _third)));
        return (int)area;
    }
}
