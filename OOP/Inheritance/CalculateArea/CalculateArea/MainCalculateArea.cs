﻿class MainCalculateArea
{
    public static void MainFunc()
    {
        Shape first = new Rectangle(2, 3);
        Console.WriteLine(first.CalculateArea());
        Shape second = new Circle(3);
        Console.WriteLine(second.CalculateArea());
        Shape third = new Triangle(6, 8, 10);
        Console.WriteLine(third.CalculateArea());
    }
}
