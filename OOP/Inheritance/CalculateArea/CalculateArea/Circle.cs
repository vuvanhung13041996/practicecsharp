﻿

class Circle : Shape
{
    int _bankinh;
    public Circle(int bankinh)
    {
        _bankinh = bankinh;
    }
    public override int CalculateArea()
    {
        return (int)(Math.Pow(_bankinh, 2) * Math.PI);
    }
}