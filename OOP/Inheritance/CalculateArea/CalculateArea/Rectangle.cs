﻿

class Rectangle : Shape
{
    public int _dai;
    int _rong;
    public Rectangle(int dai, int rong)
    {
        _dai = dai;
        _rong = rong;
    }
    public override int CalculateArea()
    {
        return _dai * _rong;
    }
}
