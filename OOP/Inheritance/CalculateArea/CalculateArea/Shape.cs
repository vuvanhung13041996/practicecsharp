﻿

abstract class Shape
{
    public abstract int CalculateArea();
}
