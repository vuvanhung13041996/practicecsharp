﻿class MainArray
{
    public static void MainFunc()
    {
        Shape[] shapes = new Shape[9];
        shapes[0] = new Circle(1);
        shapes[1] = new Circle(2);
        shapes[2] = new Circle(3);
        shapes[3] = new Triangle(4, 3, 4);
        shapes[4] = new Triangle(1, 3, 4);
        shapes[5] = new Rectangle(21, 4);
        shapes[6] = new Triangle(3, 6, 4);
        shapes[7] = new Rectangle(32, 4);
        shapes[8] = new Rectangle(45, 4);
        foreach (var item in shapes)
        {
            Console.WriteLine(item.GetType());
            Console.WriteLine(item.CalculateArea());
        }
    }
}
