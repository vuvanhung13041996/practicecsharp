﻿class Library
{
    private Book[] allBook;
    private Library()
    {

    }
    // create so luong book in library and condition
    public Library(int lenght)
    {
        if (lenght > 0)
        {
            allBook = new Book[lenght];
        }
        else
        {
            throw new Exception("Library is not empty");
        }
    }
    // create Book in library
    public void addNewBook(int ID, string Title, string Author)
    {
        //id vua la index of allBook vua la id cua class book
        if (coditionAddandGetBook(ID))
        {
            allBook[ID - 1] = new Book(ID, Title, Author);
        }
        else
        {
            throw new Exception("over Library aaaaaaaaaaaaaaaa");
        }
    }

    // codition add newBook in library
    private bool coditionAddandGetBook(int ID)
    {
        bool result = true;
        if (ID < 0 && ID >= allBook.Length)
        {
            result = false;
        }
        return result;
    }

    // Chekout
    public void ChekOut(int ID)
    {
        if (coditionAddandGetBook(--ID))
        {
            if (allBook[--ID].IsAvailable = true)
            {
                Console.WriteLine("checkout success");
                allBook[--ID].IsAvailable = false;
            }
            else
            {
                Console.WriteLine("checkout fail");
            }
        }
        else
        {
            throw new Exception("ID Khong Ton Tai");
        }
    }
    //return Book
    public void reTurnBook(int ID)
    {
        if (coditionAddandGetBook(ID))
        {
            if (allBook[ID].IsAvailable = false)
            {
                Console.WriteLine("return book success");
                allBook[ID].IsAvailable = true;
            }
            else
            {
                Console.WriteLine("book already exist you fail to input id");
            }
        }
        else
        {
            throw new Exception("ID not exist");
        }
    }

    // PrintBookInfo
    public void PrintBookInfo()
    {
        Console.WriteLine("=========");
        foreach (var item in allBook)
        {
            if (item is null)
            {
                continue;
            }
            else
            {
                Console.WriteLine("----------");
                Console.WriteLine(item.ID);
                Console.WriteLine(item.Title);
                Console.WriteLine(item.Author);
                Console.WriteLine(item.IsAvailable);
                Console.WriteLine("----------");
            }
        }
        Console.WriteLine("=========");
    }

    public void PrintBookInfoAdd(int index)
    {

        Console.WriteLine("----------");
        Console.WriteLine(allBook[index].ID);
        Console.WriteLine(allBook[index].Title);
        Console.WriteLine(allBook[index].Author);
        Console.WriteLine(allBook[index].IsAvailable);
        Console.WriteLine("----------");
    }
}
