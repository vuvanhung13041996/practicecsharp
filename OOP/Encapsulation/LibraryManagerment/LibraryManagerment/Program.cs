﻿Console.OutputEncoding = System.Text.Encoding.Default;

Console.WriteLine("Tạo Thư Viện có thể lưu chữ số lượng sách như bạn mong muốn");
int value = 0;
bool @bool = false;
// create library
do
{
    if (value < 0)
    {
        Console.WriteLine("Tạo Thư Viện có thể lưu chữ số lượng lớn hơn 0");
    }
    else if (value > 0)
    {
        Console.WriteLine("Tạo Thư Viện thành công");
    }
    @bool = int.TryParse(Console.ReadLine(), out value);
}
while (@bool is false || value < 0);

// create library
Library NewLibrary = new Library(value);
Console.WriteLine("----------------------------------------------------------------------------");
@bool = false;
// add book
try
{
    do
    {
        try
        {
            MenuAddBookForLibrary();
            int IdBook = int.Parse(Console.ReadLine());
            string NameBook = Console.ReadLine();
            string AuthorBook = Console.ReadLine();
            NewLibrary.addNewBook(IdBook, NameBook, AuthorBook);
            //id bat dau tu 1 va index bat dau tu 0 
            NewLibrary.PrintBookInfo();
            Console.WriteLine("Tiếp Tục Thêm Ấn Phím 1 Đã Xong Ấn Phím 2");
            int continueAddBookNumber = int.Parse(Console.ReadLine());
            if (continueAddBookNumber == 2)
            {
                break;
            }
            else if (continueAddBookNumber == 1)
            {
                Console.Clear();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine("Tiếp Tục Thêm Ấn Phím 1 Đã Xong Ấn Phím 2");
            int continueAddBookNumber = int.Parse(Console.ReadLine());
            if (continueAddBookNumber == 2)
            {
                break;
            }
            else if (continueAddBookNumber == 1)
            {
                MenuAddBookForLibrary();
            }
        }
    } while (@bool is false);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}

Console.WriteLine("----------------------------------------------------------------------------");
@bool = false;
NewLibrary.PrintBookInfo();
try
{
    do
    {
        MenuLibrary();
        int Choose = int.Parse(Console.ReadLine());
        if ((Choose == 1))
        { 
            NewLibrary.PrintBookInfo();
        }
        else if ((Choose == 2))
        {
            Console.WriteLine("ID BOOK");
            int ID = int.Parse(Console.ReadLine());
            NewLibrary.ChekOut(ID);
        }
        else if ((Choose == 3))
        {
            Console.WriteLine("INPUT ID BOOK FOR RETURN");
            int ID = int.Parse(Console.ReadLine());
            NewLibrary.reTurnBook(ID);
        }
        else if ((Choose == 4))
        {
            @bool = true;
        }

    } while (@bool is false);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
void MenuLibrary()
{
    Console.Clear();
    Console.WriteLine("--------------------MENU----------------------");
    Console.WriteLine("1.Xem danh sách các cuốn sách trong thư viện.");
    Console.WriteLine("2.Mượn sách: Nhập mã số của sách để mượn.");
    Console.WriteLine("3.Trả sách: Nhập mã số của sách để trả.");
    Console.WriteLine("4.Thoát chương trình.");
}
void MenuAddBookForLibrary()
{
    Console.WriteLine("thêm sách vào thư viện của bạn");
    Console.WriteLine("1. Nhập Mã số của sách");
    Console.WriteLine("2. Nhập Tên sách");
    Console.WriteLine("3. Nhập Tác giả của sách");
}