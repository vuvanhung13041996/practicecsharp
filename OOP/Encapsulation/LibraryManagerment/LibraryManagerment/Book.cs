﻿/*
 * Class Book(ID,Title,Author,Available)
 * thiet lap private
 */

class Book
{
    // instance
    private int _ID;
    private String _Title;
    private String _Author;
    private bool _Available;
    //ctor private
    private Book()
    {

    }
    //ctor public
    public Book(int Id, string Title, string Author)
    {
        _ID = Id;
        _Title = Title;
        _Author = Author;
        _Available = true;
    }
    //property instance
    public int ID
    {
        get { return _ID; }
        // no set
    }
    public string Title
    {
        get { return _Title; }
        // no set  
    }
    public String Author
    {
        get { return _Author; }
        // no set
    }
    //IsAvailable
    public bool IsAvailable
    {
        get { return _Available; }
        set { _Available = value; }
    }
}