﻿
/*
 * create class customer;
 * AccountBalance khong am, va khong duoc gan gia tri;
 * AddFunds them tien vao AccountBalance, dk (khong am);
 * MakePurchase tru di so tien chi dinh tu AccountBalance, dk(MakePurchase <AccountBalance)
 * constructor khoi tao cac gia tri;
 * CanAfford
 */
namespace CustomerNameSpace
{
    public class Customer
    {
        // field to create or change instance 
        private string _Name;
        private string _Email;
        private double _AccountBalance = 0;
        //Contructor not instance
        private Customer()
        {

        }
        public Customer(string Name, string Email)
        {
            _Name = Name;
            _Email = Email;
            //if (AccountBalance < 0)
            //{
            //    throw new ArgumentException("initial AccountBalance small than 0");
            //}
            //else
            //{
            //    _AccountBalance = AccountBalance;
            //}
        }
        //property Name
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        // property Email
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        // property AccountBalance 
        public double AccountBalance
        {
            get { return _AccountBalance; }
            // not add value
        }

        //AddFunds
        public void AddFunds(double moneyAddFunds)
        {
            if (moneyAddFunds < 0)
            {
                throw new Exception("money add small than 0");
            }
            else
            {
                _AccountBalance += moneyAddFunds;
                Console.WriteLine("succes add + ");
            }
        }
        //MakePurchase
        public void MakePurchase(double moneyMakePurchase)
        {
            if (CanAfford(moneyMakePurchase) is false)
            {
                throw new Exception("money no enough to pay");
            }
            else
            {
                _AccountBalance -= moneyMakePurchase;
                Console.WriteLine("succes -");
            }
        }
        // CanAfford
        public bool CanAfford(double money)
        {
            bool result = true;
            if (money > _AccountBalance)
            {
                result = false;
            }
            return result;
        }
    }
}