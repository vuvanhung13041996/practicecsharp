﻿Console.WriteLine("Input Number Positive, The Application Will Show You Result Sum 1 To N");
Console.WriteLine("Input Your Number");
int number = int.Parse(Console.ReadLine());

// solution 1
int result = 0;
//condition
int i = 0;
do
{
    result += i;
    ++i;
} while (i < number + 1);
Console.WriteLine(result);


//solution 2
Console.WriteLine("-------------------------");
result = 0;
for (int j = 0; j <= number; j++)
{
    result += j;
}
Console.WriteLine(result);
Console.WriteLine("-------------------------");

//solution 3
//codition
result = 0;
int t = 0;
while (t <= number)
{
    result += t;
    ++t;
}
Console.WriteLine(result);
Console.WriteLine("-------------------------------------------------------------------------");


result = 1;
//codition giai thua 1 
int first = number;
do
{
    result *= first;
    --first;
} while (first > 0);
Console.WriteLine("giai thua la {0}", result);
Console.WriteLine("-------------------------------------------------------------------------");



result = 1;
//codition giai thua 2 
first = number;
for (int k = first; k > 0; k--)
{
    result *= k;
}
Console.WriteLine("giai thua la {0}", result);
Console.WriteLine("-------------------------------------------------------------------------");


result = 1;
//codition giai thua 3 
first = number;
while (first > 0)
{
    result *= first;
    --first;
}
Console.WriteLine("giai thua la {0}", result);
Console.WriteLine("-------------------------------------------------------------------------");

Console.WriteLine("---------------------------------Fibonacci----------------------------");

int firstnumber = 0;
int secondnumber = 1;
int thirdnumber = 0;

for (int q = 0; q < number - 1; q++)
{
    if (q == 0)
    {
        Console.WriteLine(firstnumber);
        Console.WriteLine(secondnumber);
    }
    else
    {
        thirdnumber = firstnumber + secondnumber;
        Console.WriteLine(thirdnumber);
        firstnumber = secondnumber;
        secondnumber = thirdnumber;
    }
}

