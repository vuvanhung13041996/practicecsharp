﻿using System.ComponentModel;

Console.WriteLine("the Scissors Rock Paper Spock Lizard Game!");
Console.WriteLine("|-------------|");
Console.WriteLine("1 is Scissors");
Console.WriteLine("2 is Rock");
Console.WriteLine("3 is Paper");
Console.WriteLine("4 is Spock");
Console.WriteLine("5 is Lizard");
Console.WriteLine("|-------------|");

// tow person choose;
Console.WriteLine("Person1 choose");
int Person1 = int.Parse(Console.ReadLine());
if (1 > Person1 || Person1 > 5)
{
    Console.WriteLine("Choose Lager Than 0 And Small Than 6");
    Person1 = int.Parse(Console.ReadLine());
}

Console.WriteLine("Person2 choose");
int Person2 = int.Parse(Console.ReadLine());
if (1 > Person2 || Person2 > 5)
{
    Console.WriteLine("Choose Lager Than 0 And Small Than 6");
    Person2 = int.Parse(Console.ReadLine());
}


//text corresponding
string Scissors = "Scissors";
string Rock = "Rock";
string Paper = "Paper";
string Spock = "Spock";
string Lizard = "Lizard";

// switch case return person choose what?
void whatIsPersonChoose(int number, string person)
{
    Console.WriteLine("{0} Choose Is : ", person);
    switch (number)
    {
        case 1:
            Console.WriteLine(Scissors);
            break;
        case 2:
            Console.WriteLine(Rock);
            break;
        case 3:
            Console.WriteLine(Paper);
            break;
        case 4:
            Console.WriteLine(Spock);
            break;
        case 5:
            Console.WriteLine(Lizard);
            break;
        default:
            break;
    }
}
whatIsPersonChoose(Person1, "Person1");
whatIsPersonChoose(Person2, "Person2");


// boolean Person Win
bool Person1Win =
    (Person1 == 1 && (Person2 == 3 || Person2 == 5))
    || (Person1 == 2 && (Person2 == 1 || Person2 == 5))
    || (Person1 == 3 && (Person2 == 2 || Person2 == 4))
    || (Person1 == 4 && (Person2 == 2 || Person2 == 1))
    || (Person1 == 5 && (Person2 == 3 || Person2 == 4));
// result the game
if (Person1Win)
{
    Console.WriteLine("Person 1 Win");
}
else if (Person1 == Person2)
{
    Console.WriteLine("2 Person Equal");
}
else
{
    Console.WriteLine("Person 2 Win");
}
