﻿
// instruct (guide)
Console.WriteLine("The Scissors Rock Paper Game");
Console.WriteLine("1 Coresponding With Scissors");
Console.WriteLine("2 Coresponding With Rock");
Console.WriteLine("3 Coresponding With Paper");

// result string coresponding with value input 1,2,3
string resultFirstPerson = "";
string resultSecondPerson = "";
string scissors = "Scissors";
string rock = "Rock";
string paper = "Paper";

// first person choose
Console.WriteLine("The Fist Person Choose");
int firstPerson = int.Parse(Console.ReadLine());


// rule of number choose
void ruleFuncFirstChoose()
{
    switch (firstPerson)
    {
        case 1:
            resultFirstPerson = scissors;
            break;
        case 2:
            resultFirstPerson = rock;
            break;
        case 3:
            resultFirstPerson = paper;
            break;
        default:
            // if not correct with rule, the person first obligatory rechoose
            Console.WriteLine("You Happy Choose Value 1 Or 2 Or 3");
            firstPerson = int.Parse(Console.ReadLine());
            ruleFuncFirstChoose();
            break;

    }
}
ruleFuncFirstChoose();

// second person choose
Console.WriteLine("The Second Person Choose");
int secondPerson = int.Parse(Console.ReadLine());
// rule of number choose
void ruleFuncSecondChoose()
{
    switch (secondPerson)
    {
        case 1:
            resultSecondPerson = scissors;
            break;
        case 2:
            resultSecondPerson = rock;
            break;
        case 3:
            resultSecondPerson = paper;
            break;
        default:
            // if not correct with rule, the person first obligatory rechoose
            Console.WriteLine("You Happy Choose Value 1 Or 2 Or 3");
            secondPerson = int.Parse(Console.ReadLine());
            ruleFuncSecondChoose();
            break;

    }
}

ruleFuncSecondChoose();

/*
 * rule of the game
 * 1 Win 3
 * 2 Win 1
 * 3 Win 2
 * Else Draw
 */
bool firstPersonWin =
    (firstPerson == 1 && secondPerson == 3)
    || (firstPerson == 2 && secondPerson == 1)
    || (firstPerson == 3 && secondPerson == 2);
bool secondPersonWin =
    (secondPerson == 1 && firstPerson == 3)
    || (secondPerson == 2 && firstPerson == 1)
    || (secondPerson == 3 && firstPerson == 2);


//result of the game 
if (firstPersonWin)
{
    Console.WriteLine("Fist Person Win");
    Console.WriteLine("Because Firt Chose {0}", resultFirstPerson);
    Console.WriteLine("And Second Chose {0}", resultSecondPerson);
}
else if (secondPersonWin)
{
    Console.WriteLine("Second Person Win");
    Console.WriteLine("Because Firt Chose {0}", resultFirstPerson);
    Console.WriteLine("And Second Chose {0}", resultSecondPerson);
}
else
{
    Console.WriteLine("Two Person Draw");
    Console.WriteLine("Because Firt Chose {0}", resultFirstPerson);
    Console.WriteLine("And Second Chose {0}", resultSecondPerson);
}