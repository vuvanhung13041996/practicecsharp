﻿using static System.Runtime.InteropServices.JavaScript.JSType;

Console.WriteLine("input three number application will show you more");
Console.WriteLine("first number");
int numberFirst = int.Parse(Console.ReadLine());
Console.WriteLine("second number");
int numberSecond = int.Parse(Console.ReadLine());
Console.WriteLine("third number");
int numberThird = int.Parse(Console.ReadLine());


/*
    *positive
    *negative
    *zero
*/

Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
void funcNegativePositiveOr0(int number, string text)
{
    Console.WriteLine(text);
    if (number > 0)
    {
        Console.WriteLine("{0} is Positive", number);
    }
    else if (numberFirst < 0)
    {
        Console.WriteLine("{0} is Negative", number);
    }
    else
    {
        Console.WriteLine("{0} is 0", number);
    }
}

funcNegativePositiveOr0(numberFirst, "numberFirst");
funcNegativePositiveOr0(numberSecond, "numberSecond");
funcNegativePositiveOr0(numberThird, "numberThird");


//compare three number
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
void compareNumber(int first, int second)
{
    if (first < second)
    {
        Console.WriteLine("first less than second");
    }
    else if (first > second)
    {
        Console.WriteLine("first greater than second");
    }
    else
    {
        Console.WriteLine("two number equal");
    }
}

compareNumber(numberFirst, numberSecond);

// rule Even,odd
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
Console.WriteLine("-----------------------------------------------");
void ruleEvenOdd(int number, string text)
{
    Console.WriteLine(text + " is");
    if (number % 2 == 0 && number % 3 == 0)
    {
        Console.WriteLine("Even");
        Console.WriteLine("number chia het cho 2 va 3");
    }
    else if (number % 2 == 0)
    {
        Console.WriteLine("Even");
        Console.WriteLine("number chia het cho 2");
    }
    else if (number % 3 == 0)
    {
        Console.WriteLine("odd");
        Console.WriteLine("number chia het cho 3");
    }
    else
    {
        Console.WriteLine("odd");
    }
}
ruleEvenOdd(numberFirst, "numberFirst");
ruleEvenOdd(numberSecond, "numberSecond");
ruleEvenOdd(numberThird, "numberThird");
